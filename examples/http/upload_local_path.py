import requests
import json

# THOSE ARE TEST CREDENTIALS.
# NEVER USE THEM OUTSIDE EXAMPLES!!!
workspace_token = "73f19f87-f741-4f53-ae50-a9272dc87ea7"
user_token = "7c817d0d8f0e2b719a9df798fbdefe75cf5ba4be"
from pathlib import Path
import hashlib

def md5sum_hash(fpath):
        file_hash = hashlib.md5()
        with open(fpath, 'rb') as f:
            chunk = f.read(8192)
            while chunk:
                file_hash.update(chunk)
                chunk = f.read(8192)
        md5 = file_hash.hexdigest()
        return md5


def upload_file():
    filename = "../dft.txt"
    p = Path(filename).resolve()

    hash = md5sum_hash(p)
    files = {"file": open(filename, "rb")}
    meta_str =  json.dumps({"howuploaded":"local"})
    content = requests.post(
        "http://localhost:8000/api/file",
        params={"token": workspace_token},
        data={
            "local": True,
            "local_path": p,
            "metadata": meta_str,
            "hash": hash
        },
        headers={"Authorization": "Token " + user_token},
    )
    print("--------------REQUEST (HTTP)----------------")
    print(content.request.url)
    print(content.request.body)
    print(content.request.headers)
    print()
    print("--------------RESPONSE (JSON)----------------")
    # print(content.json())
    print()
    print("--------------RESPONSE----------------")
    print(str(content.headers))
    print(str(content.content))
    print("--------------------------------------")
    print(str(content.request.body))
    if content.status_code == 200:
        print("File uploaded successfully")
    else:
        raise ValueError


if __name__ == "__main__":
    upload_file()
