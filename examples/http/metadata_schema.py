
import requests
import json

# THOSE ARE TEST CREDENTIALS.
# NEVER USE THEM OUTSIDE EXAMPLES!!!
workspace_token = "73f19f87-f741-4f53-ae50-a9272dc87ea7"
user_token = "7c817d0d8f0e2b719a9df798fbdefe75cf5ba4be"


def make_schema():
    filename = "../exampleFile.txt"
    files = {"file": open(filename, "rb")}
    schema = {
        "type" : "object",
        "properties" : {
            "price" : {"type" : "number"},
            "name" : {"type" : "string"},
        },
    }
    meta_schema =  json.dumps(schema)
    content = requests.post(
        "http://localhost:8000/api/metaschema",
        params={"token": workspace_token},
        data={"metadata_schema": meta_schema, "filetype": "atype"},
        files=files,
        headers={"Authorization": "Token " + user_token},
    )
    print("--------------REQUEST (HTTP)----------------")
    print(content.request.url)
    print(content.request.body)
    print(content.request.headers)
    print()
    print("--------------RESPONSE----------------")
    print(str(content.headers))
    print(str(content.content))
    print("--------------RESPONSE (JSON)----------------")
    try:
        print(content.json())
        print()
    except:
        print ("COULDNT READ AS JSON")
    print("--------------------------------------")
    print(str(content.request.body))
    if content.status_code == 200:
        print("File uploaded successfully")
    else:
        raise ValueError

def upload_with_valid_schema(valid=True):
    filename = "../exampleFile.txt"
    files = {"file": open(filename, "rb")}
    price = 34.99 if valid else "wrongvalue"
    meta_str =  json.dumps({"name" : "Eggs", "price" : price})
    content = requests.post(
        "http://localhost:8000/api/file",
        params={"token": workspace_token},
        data={"path": filename, "metadata":meta_str, "filetype":"atype"},
        files=files,
        headers={"Authorization": "Token " + user_token},
    )
    print("--------------REQUEST (HTTP)----------------")
    print(content.request.url)
    print(content.request.body)
    print(content.request.headers)
    print()
    print("--------------RESPONSE (JSON)----------------")
    print(content.json())
    if not valid:
        print("Invalidity reason:")
        print(content.json()['cause'])
    print()
    print("--------------RESPONSE----------------")
    print(str(content.headers))
    print(str(content.content))
    print("--------------------------------------")
    print(str(content.request.body))
    if content.status_code == 200:
        print("File uploaded successfully")
    elif content.status_code == 400 and not valid:
        print("Upload failed successfully")
    else:
        raise ValueError

def list_schemas():
    print()
    content = requests.get(
        "http://localhost:8000/api/getschema",
        params={"token": workspace_token},
        headers={"Authorization": "Token " + user_token},
    )
    print("--------------REQUEST (HTTP)----------------")
    print(content.request.url)
    print(content.request.body)
    print(content.request.headers)
    print()
    print("--------------RESPONSE (JSON)----------------")
    try:
        print(content.json())
        print()
    except:
        print("not a json")
    print("--------------RESPONSE----------------")
    print(str(content.headers))
    print(str(content.content))
    print("--------------------------------------")


if __name__ == "__main__":
    make_schema()
    upload_with_valid_schema()
    upload_with_valid_schema(valid=False)
    list_schemas()
