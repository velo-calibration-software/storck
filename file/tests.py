from django.test import TestCase
from django.core.files import File
from django.contrib.auth.models import User
from workspace.models import Workspace, WorkspaceToken, MetadataSchemas
from rest_framework.test import APITestCase, URLPatternsTestCase, APIClient
from rest_framework.authtoken.models import Token
from django.urls import reverse, path, include
import random
import string
import tempfile
import os
import tempfile
import json
import jsonschema
import datetime
from file.models import StorckFile
from file.views import LocalFileError, FileView
import file.views
from file.utils import hash_data, hash_file
from unittest.mock import MagicMock, patch, mock_open, PropertyMock
from pathlib import Path



class GeneralFileTest:
    def setup_schema(self):
        self.schema = {
            "type" : "object",
            "properties" : {
                "price" : {"type" : "number"},
                "name" : {"type" : "string"},
            },
        }

        self.schema2 = {
            "type" : "object",
            "properties" : {
                "price" : {"type" : "number"},
                "name" : {"type" : "string"},
                "city" : {"type" : "string"},
            },
        }
        self.invalid_schema = {
            "type" : "object",
            "properties" : {
                "price" : {"type" : "number"},
                "name" : {"type" : "ggggggggggggggggg"},
            },
        }

        self.valid_meta_dict = {"name" : "Eggs", "price" : 34.99}
        self.invalid_meta_dict = {"name" : "Eggs", "price" : "wrongvalue"}
        self.valid_meta_str =  json.dumps(self.valid_meta_dict)
        self.invalid_meta_str =  json.dumps(self.invalid_meta_dict)
        self.schema_str = json.dumps(self.schema)
        self.schema2_str = json.dumps(self.schema2)
        self.invalid_schema_str = json.dumps(self.invalid_schema)
        self.filetype = "atype"
        self.schema_obj1 = MetadataSchemas.objects.create(
            workspace= self.workspace_obj,
            user = self.user_obj,
            schema = self.schema,
            filetype = self.filetype,
        )

    def setUp(self):
        self.email = "foo@bar.com"
        self.user_obj = User.objects.create_user(username="test", email=self.email)
        self.user_obj2 = User.objects.create_user(username="test2", email=self.email)
        self.password = "some_password"
        user_token = "7c817d0d8f0e2b719a9df798fbdefe75cf5ba4be"
        self.token = Token.objects.create(user=self.user_obj, key=user_token)
        user_token2 = "bbbbbd0d8f0e2b719a9df798fbdefe75cf5ba4be"
        self.token2 = Token.objects.create(user=self.user_obj2, key=user_token2)
        self.user_obj.set_password(self.password)
        self.user_obj.save()
        self.workspace_obj = Workspace.objects.create()
        self.workspace_obj.save()
        self.workspace_obj.users.add(self.user_obj)
        self.workspace_obj.save()
        workspace_token = "73f19f87-f741-4f53-ae50-a9272dc87ea7"
        self.wrksp_token = WorkspaceToken.objects.create(
            workspace=self.workspace_obj, name="Default token", token=workspace_token,
        )

        self.workspace_obj2 = Workspace.objects.create()
        self.workspace_obj2.save()
        self.workspace_obj2.users.add(self.user_obj2)
        self.workspace_obj2.save()
        workspace_token2 = "aaaa9f87-f741-4f53-ae50-a9272dc87ea7"
        self.wrksp_token2 = WorkspaceToken.objects.create(
            workspace=self.workspace_obj2,
            name="Default token",
            token=workspace_token2,
        )

        self.setup_schema()
        f = open("examples/exampleFile.txt")
        example_file = File(f)
        StorckFile.objects.create(
            file=example_file, user=self.user_obj, workspace=self.workspace_obj
        )
        self.file_obj = StorckFile.objects.first()
        self.file_obj2 = StorckFile.objects.create(
            file=example_file,
            user=self.user_obj,
            workspace=self.workspace_obj,
            stored_path="stored/path",
            metadata="some data",
            meta_closed="some different data",
            filetype=self.filetype,
            hide=True,
        )
        self.generate_random_string = lambda x: "".join(
            random.choices(string.ascii_uppercase + string.digits, k=x)
        )



class FileModelTestCase(TestCase, GeneralFileTest):
    def __init__(self, *args, **kwargs):
        TestCase.__init__(self, *args, **kwargs)
        GeneralFileTest.__init__(self)

    def setUp(self):
        GeneralFileTest.setUp(self)

    def test_model_return_correct_file(self):
        file_path = StorckFile.objects.first().file
        self.assertEqual("example" in str(file_path), True)

    def test_attributes(self):
        f = StorckFile.objects.first()
        self.assertTrue(hasattr(f, "id"))
        self.assertTrue(hasattr(f, "file"))
        self.assertTrue(hasattr(f, "hash"))
        self.assertTrue(hasattr(f, "workspace"))
        self.assertTrue(hasattr(f, "user"))
        self.assertTrue(hasattr(f, "previous_version"))
        self.assertTrue(hasattr(f, "duplicate_of"))
        self.assertTrue(hasattr(f, "date"))
        self.assertTrue(hasattr(f, "stored_path"))
        self.assertTrue(hasattr(f, "metadata"))
        self.assertTrue(hasattr(f, "meta_closed"))
        self.assertTrue(hasattr(f, "hide"))
        self.assertTrue(hasattr(f, "filetype"))

    def test_hash_data(self):
        random_string1 = self.generate_random_string(100).encode("utf-8")
        random_string2 = self.generate_random_string(100).encode("utf-8")
        if random_string1 == random_string2:
            random_string2 = self.generate_random_string(100).encode("utf-8")
        hash1a = hash_data(random_string1)
        hash1b = hash_data(random_string1)
        hash2 = hash_data(random_string2)
        self.assertNotEqual(hash1a, hash2)
        self.assertEqual(hash1a, hash1b)

    def test_hash_file(self):
        random_string1 = self.generate_random_string(100)
        random_string2 = self.generate_random_string(100)
        if random_string1 == random_string2:
            random_string2 = self.generate_random_string(100)
        tmpfile1a = tempfile.NamedTemporaryFile("w", delete=False)
        tmpfile1a.write(random_string1)
        filepath1a = tmpfile1a.name
        tmpfile1b = tempfile.NamedTemporaryFile("w", delete=False)
        tmpfile1b.write(random_string1)
        filepath1b = tmpfile1b.name
        tmpfile2 = tempfile.NamedTemporaryFile("w", delete=False)
        tmpfile2.write(random_string2)
        filepath2 = tmpfile2.name
        tmpfile1a.close()
        tmpfile1b.close()
        tmpfile2.close()
        hash1a = hash_file(filepath1a)
        hash1b = hash_file(filepath1b)
        hash2 = hash_file(filepath2)
        self.assertEqual(hash1b, hash1a)
        self.assertNotEqual(hash2, hash1a)
        os.remove(tmpfile1a.name)
        os.remove(tmpfile1b.name)
        os.remove(tmpfile2.name)


    def test_filetype1(self):
        self.assertIsNone(self.file_obj.filetype)

    def test_filetype2(self):
        self.assertEqual(self.file_obj2.filetype, "atype")

    def test_hash(self):
        self.assertIsNone(self.file_obj.hash)

    def test_workspace(self):
        self.assertEqual(self.file_obj.workspace, self.workspace_obj)

    def test_user(self):
        self.assertEqual(self.file_obj.user, self.user_obj)

    def test_previous_version(self):
        # @TODO this is missing the actual test case now, as setting this
        # is moved into api
        self.assertIsNone(self.file_obj.previous_version)

    def test_duplicate_of(self):
        # @TODO this is missing the actual test case now, as setting this
        # is moved into api
        self.assertIsNone(self.file_obj.duplicate_of)

    def test_date(self):
        diff = self.file_obj.date - datetime.datetime.now(datetime.timezone.utc)
        self.assertGreater(60, diff.total_seconds())

    def test_stored_path(self):
        # @TODO this is missing the actual test case now, as setting this
        # is moved into api
        self.assertIsNone(self.file_obj.stored_path)
        self.assertEqual(self.file_obj2.stored_path, "stored/path")

    def test_metadata(self):
        self.assertIsNone(self.file_obj.metadata)
        self.assertEqual(self.file_obj2.metadata, "some data")

    def test_meta_closed(self):
        self.assertIsNone(self.file_obj.meta_closed)
        self.assertEqual(self.file_obj2.meta_closed, "some different data")

    def test_hide(self):
        self.assertFalse(self.file_obj.hide)
        self.assertTrue(self.file_obj2.hide)


class FileViewTests(APITestCase, URLPatternsTestCase, GeneralFileTest):
    urlpatterns = [
        path("api/", include("file.urls")),
    ]

    def __init__(self, *args, **kwargs):
        APITestCase.__init__(self, *args, **kwargs)
        URLPatternsTestCase.__init__(self, *args, **kwargs)
        GeneralFileTest.__init__(self)

    def setUp(self):
        GeneralFileTest.setUp(self)

    def test_get_by_path_list(self):
        # client.login(email=self.email, password=self.password)
        self.client.credentials(HTTP_AUTHORIZATION="Token " + self.token.key)
        response = self.client.get(reverse("all-files"),)
        self.assertEqual(response.status_code, 403)
        self.assertEqual(response.data, {"message": "Missing workspace token"})
        token = WorkspaceToken.objects.filter(workspace=self.workspace_obj).first()
        # breakpoint()
        response2 = self.client.get(reverse("all-files"), {"token": token.token})
        queryset = StorckFile.objects.filter(
            workspace=self.workspace_obj, hide=False
        ).values()
        self.assertDictEqual({"files": list(queryset)}, response2.data)

        response3 = self.client.get(
            reverse("all-files"), {"token": token.token, "hidden": True}
        )
        queryset = StorckFile.objects.filter(workspace=self.workspace_obj).values()
        self.assertDictEqual({"files": list(queryset)}, response3.data)

    def test_info(self):
        self.client.credentials(HTTP_AUTHORIZATION="Token " + self.token.key)
        response = self.client.get(
            reverse("info-file"),
            {"token": self.wrksp_token.token, "path": "df;lkj;lkjw;lkj"},
        )
        self.assertEqual(response.status_code, 404)
        self.assertEqual(response.data, {"message": "File not found"})

        response = self.client.get(
            reverse("info-file"),
            {"token": self.wrksp_token.token, "path": self.file_obj2.stored_path},
        )
        self.assertEqual(response.status_code, 200)

    def test_get_file(self):
        self.client.credentials(HTTP_AUTHORIZATION="Token " + self.token.key)
        response = self.client.get(reverse("single-file"),)
        self.assertEqual(response.status_code, 403)
        self.assertEqual(response.data, {"message": "Missing workspace token"})

        response = self.client.get(
            reverse("single-file"), {"token": self.wrksp_token.token}
        )
        self.assertEqual(response.status_code, 400)
        self.assertEqual(response.data, {"message": "Missing file id"})

        response = self.client.get(
            reverse("single-file"),
            {"token": self.wrksp_token.token, "id": self.file_obj2.id},
        )
        self.assertEqual(response.status_code, 200)
        fp = self.file_obj2.file
        data = fp.open().read()
        self.assertEqual(response.content, data)
        fp.close()

    def test_post_file(self):
        self.client.credentials(HTTP_AUTHORIZATION="Token " + self.token.key)
        response = self.client.post(reverse("single-file"),)
        self.assertEqual(response.status_code, 400)
        self.assertEqual(response.data, {"message": "no file specified"})

    @patch("file.views.settings.REQUIRE_HASH", new=True)
    @patch("file.views.hash_file", new=lambda x: "aaaa")
    def test_process_local_file(self):
        tmpfile_path = "tmp1.txt"
        request = MagicMock()
        request.POST = {"hash": "aaaa"}
        local_hash = "aaaa"
        with tempfile.TemporaryDirectory() as tmpdirname:
            with patch("file.views.media_root_path", new=Path(tmpdirname)):
                error_msg = "local file: \[{}\] not found, make sure that the path is absolute"\
                "and storck can access the file"
                error_txt = error_msg.format(tmpfile_path)
                with self.assertRaisesRegex(
                    LocalFileError, error_txt
                ) as err:
                    result = FileView.process_local_file(
                        None, request, Path(tmpfile_path)
                    )
                self.assertFalse((Path(tmpdirname) / local_hash).is_file())

                tmpfile_path = Path(tmpdirname) / "tmp1.txt"
                with open(tmpfile_path, "w") as f1:
                    f1.write("test1")
                request = MagicMock()
                request.POST = {"hash": "bbbb"}
                with patch("file.views.settings.REQUIRE_HASH", new=True):
                    with self.assertRaisesRegex(
                        LocalFileError, "given hash bbbb is not matching sent hash"
                    ) as err:
                        result = FileView.process_local_file(
                            None, request, tmpfile_path
                        )
                self.assertFalse((Path(tmpdirname) / local_hash).is_file())


                request.POST = {"hash": "aaaa"}
                with patch("file.views.settings.REQUIRE_HASH", new=True):
                    result = FileView.process_local_file(
                        None, request, tmpfile_path
                    )
                self.assertEquals(result, request.POST["hash"])
                self.assertTrue((Path(tmpdirname) / local_hash).is_file())
                self.assertTrue(tmpfile_path.exists())

                with open(tmpfile_path, "w") as f1:
                    f1.write("test1")
                with patch("file.views.settings.REQUIRE_HASH", new=True):
                    result = FileView.process_local_file(
                        None, request, tmpfile_path
                    )
                self.assertEquals(result, request.POST["hash"])
                self.assertTrue(tmpfile_path.exists())

    def test_post_without_file(self):
        self.client.credentials(HTTP_AUTHORIZATION="Token " + self.token.key)
        response = self.client.post(
            reverse("single-file") + "?token={}".format(self.wrksp_token.token),
        )

        self.assertEqual(response.status_code, 400)

    def test_post_with_file(self):
        self.client.credentials(HTTP_AUTHORIZATION="Token " + self.token.key)
        with open("examples/exampleFile3.txt", "rb") as output:
            response = self.client.post(
                reverse("single-file") + "?token={}".format(self.wrksp_token.token),
                {"path": "stored/path0", "file": output},
            )
        self.assertEqual(response.status_code, 200)
        self.assertFalse(response.data["duplicate"])
        self.assertIsNone(response.data["previous_version"])

    def test_post_with_duplicate(self):
        self.client.credentials(HTTP_AUTHORIZATION="Token " + self.token.key)
        with open("examples/exampleFile3.txt", "rb") as output:
            response = self.client.post(
                reverse("single-file") + "?token={}".format(self.wrksp_token.token),
                {"path": "stored/path0", "file": output},
            )
        with open("examples/exampleFile3.txt", "rb") as output:
            response = self.client.post(
                reverse("single-file") + "?token={}".format(self.wrksp_token.token),
                {"path": "stored/path2", "file": output},
            )
        self.assertEqual(response.status_code, 200)
        self.assertTrue(response.data["duplicate"])
        self.assertIsNone(response.data["previous_version"])

    def test_post_with_previous(self):
        self.client.credentials(HTTP_AUTHORIZATION="Token " + self.token.key)
        with open("examples/exampleFile3.txt", "rb") as output:
            response = self.client.post(
                reverse("single-file") + "?token={}".format(self.wrksp_token.token),
                {"path": "stored/path2", "file": output},
            )
        previd = response.data["id"]
        with open("examples/exampleFile2.txt", "rb") as output:
            response = self.client.post(
                reverse("single-file") + "?token={}".format(self.wrksp_token.token),
                {"path": "stored/path2", "file": output},
            )
        self.assertEqual(response.status_code, 200)
        self.assertFalse(response.data["duplicate"])
        self.assertEqual(response.data["previous_version"], previd)

    @patch("file.views.DjangoFile")
    @patch("file.views.shutil")
    @patch("file.views.hash_chunked_file", new=lambda x: "filehash1")
    def test_process_file(self, new_shutil, djangofile_class_mock):
        request = MagicMock()
        request.POST = {"local_path": "", "local": False}
        filemock = MagicMock()
        filemock.name = "name"
        filemock.open.return_value = "open1"
        request.FILES = {"file": filemock}
        djangofile_class_mock.return_value = "djangofile"
        slf = MagicMock()
        slf.process_local_file = MagicMock()
        slf.process_local_file.return_value = "filehash2"
        new_shutil.move = MagicMock()
        with patch("builtins.open", mock_open()) as mock_file:
            ruploaded_file, rfile_hash, stored_path = FileView.process_file(
                slf, request
            )
            self.assertEqual(rfile_hash, "filehash1")
            self.assertEqual(ruploaded_file, "djangofile")
            self.assertEqual(stored_path, "name")

            request = MagicMock()
            request.POST = {"local_path": "/local/path", "local": True}
            request.FILES = {}
            ruploaded_file, rfile_hash, stored_path = FileView.process_file(
                slf, request
            )
            self.assertEqual(rfile_hash, "filehash2")
            self.assertEqual(ruploaded_file, "djangofile")
            self.assertEqual(stored_path, "/local/path")
            new_shutil.move.called_once()

            request = MagicMock()
            request.POST = {}
            request.FILES = {}
            with self.assertRaisesRegex(LocalFileError, "no file specified") as err:
                ruploaded_file, rfile_hash, stored_path = FileView.process_file(
                    slf, request
                )


    @patch('jsonschema.validate')
    @patch('workspace.views.MetadataSchemas.objects')
    def test_check_metadata_schema(self, schemas_cls, validate):
        # check when empty filetype
        slf = MagicMock()
        request = MagicMock()
        request.POST = {
            "metadata": self.valid_meta_str
        }
        workspace = MagicMock()
        result = FileView.check_metadata_schema(slf, request, workspace)
        self.assertEquals(result, self.valid_meta_dict)
        self.assertFalse(validate.called)

        # check when empty metadata
        request = MagicMock()
        request.POST = {
            "filetype": "atype"
        }
        sch = MagicMock()
        sch.exists.return_value = False
        filt = MagicMock()
        filt.return_value = sch
        schemas_cls.return_value = filt
        with self.assertRaises(file.views.MetadataEmpty):
            FileView.check_metadata_schema(slf, request, workspace)
            filt.assert_called_with(workspace=workspace, filetype="atype")
        self.assertFalse(validate.called)


        # check when both present
        request.POST = {
            "metadata": self.valid_meta_str,
            "filetype": "atype"
        }
        sch = MagicMock()
        sch.exists.return_value = True
        prosh =  PropertyMock("testschema")
        sch.first.return_value.schema = prosh
        schemas_cls.filter.return_value = sch
        validate.return_value = "some"
        with patch('json.loads') as loads:
            loads.return_value = "metadata1"
            result = FileView.check_metadata_schema(slf, request, workspace)
            loads.assert_called_with(self.valid_meta_str)
            validate.assert_called_with(instance="metadata1", schema=prosh)
            self.assertEqual(result, "metadata1")


    @patch('workspace.views.MetadataSchemas.objects')
    def test_check_metadata_schema_json_decode_error(self, schemas_cls):
        slf = MagicMock()
        request = MagicMock()
        workspace = MagicMock()
        request.POST = {
            "metadata": "{notajson}",
            "filetype": "atype"
        }
        sch = MagicMock()
        sch.exists.return_value = True
        sch.first.return_value.schema = PropertyMock("testschema")
        schemas_cls.filter.return_value = sch
        with self.assertRaises(json.JSONDecodeError):
            result = FileView.check_metadata_schema(slf, request, workspace)


    @patch('workspace.views.MetadataSchemas.objects')
    def test_check_metadata_schema_validation_error(self, schemas_cls):
        slf = MagicMock()
        request = MagicMock()
        workspace = MagicMock()
        request.POST = {
            "metadata": self.invalid_meta_str,
            "filetype": "atype"
        }
        sch = MagicMock()
        sch.exists.return_value = True
        sch.first.return_value.schema = self.schema
        schemas_cls.filter.return_value = sch
        with self.assertRaises(jsonschema.exceptions.ValidationError):
            result = FileView.check_metadata_schema(slf, request, workspace)



    @patch('workspace.views.WorkspaceToken.objects')
    @patch('workspace.views.Workspace.objects')
    @patch('file.models.StorckFile')
    @patch('os.path.basename')
    def test_post_metadata(self, worten, worksp, SF, ospb):
        with patch.object(FileView, "process_file") as pro_file,\
        patch.object(FileView, "process_transaction") as pro_tet,\
        patch.object(FileView, "check_metadata_schema") as check_met:
            ospb.return_value = "uploaded_file"
            uploaded_file = MagicMock()
            uploaded_file.name =  PropertyMock("uploaded_file")
            pro_file.return_value = (uploaded_file, "file_hash","stored_path")
            workspace = PropertyMock("workspace")
            workspace.pk = PropertyMock("pk")
            worten.get.return_value.workspace = workspace
            worksp.filter.return_value.exists.return_value = True
            check_met.return_value = "meta"
            error_data = {"message": "invalid metadata"}
            previous = MagicMock()
            previous.id.return_value = "prev_id"
            pro_tet.return_value = ("dupl_r", "dupl_f", previous)
            sfo = MagicMock()
            sfo.id.return_value = "sfo_id"
            sfo.duplicate_of.return_value = "duplicate_id"
            SF.return_value = sfo



            check_met.side_effect = json.JSONDecodeError("aa","b",0)
            self.client.credentials(HTTP_AUTHORIZATION="Token " + self.token.key)
            with open("examples/exampleFile3.txt", "rb") as output:
                request =  {
                    "path": "stored/path2",
                    "file": output}
                response = self.client.post(
                    reverse("single-file") + "?token={}".format(self.wrksp_token.token),
                    request
                )
                self.assertEqual(response.status_code, 400)
                ca = {"cause":  "error when parsing JSON metadata"}
                mes = {**error_data, **ca}
                self.assertEqual(response.data, mes)


            check_met.side_effect = jsonschema.exceptions.ValidationError("===")
            self.client.credentials(HTTP_AUTHORIZATION="Token " + self.token.key)
            with open("examples/exampleFile3.txt", "rb") as output:
                request =  {
                    "path": "stored/path2",
                    "file": output}
                response = self.client.post(
                    reverse("single-file") + "?token={}".format(self.wrksp_token.token),
                    request
                )
                self.assertEqual(response.status_code, 400)
                ca = {"cause":  "invalid metadata: \n ==="}
                mes = {**error_data, **ca}
                self.assertEqual(response.data, mes)


            from file.views import MetadataEmpty
            check_met.side_effect = MetadataEmpty()
            self.client.credentials(HTTP_AUTHORIZATION="Token " + self.token.key)
            with open("examples/exampleFile3.txt", "rb") as output:
                request =  {
                    "path": "stored/path2",
                    "file": output}
                response = self.client.post(
                    reverse("single-file") + "?token={}".format(self.wrksp_token.token),
                    request
                )
                self.assertEqual(response.status_code, 400)
                ca = {"cause": "metadata field empty, but this filetype requres schema"}
                mes = {**error_data, **ca}
                self.assertEqual(response.data, mes)


            from file.views import MetadataEmpty
            check_met.side_effect = MetadataEmpty()
            self.client.credentials(HTTP_AUTHORIZATION="Token " + self.token.key)
            with open("examples/exampleFile3.txt", "rb") as output:
                request =  {
                    "path": "stored/path2",
                    "file": output}
                response = self.client.post(
                    reverse("single-file") + "?token={}".format(self.wrksp_token.token),
                    request
                )
                self.assertEqual(response.status_code, 400)
                ca = {"cause": "metadata field empty, but this filetype requres schema"}
                mes = {**error_data, **ca}
                self.assertEqual(response.data, mes)

    def test_post(self):
        pass
        # self.client.credentials(HTTP_AUTHORIZATION="Token " + self.token.key)
        # # TODO test when file_hash is none
        # from file.views import Response

        # slf = MagicMock()
        # request = MagicMock()
        # slf.process_file = MagicMock()
        # slf.process_file.return_value = (None , None)
        # slf.process_file.side_effect = LocalFileError('test1')
        # rval = FileView.post(slf, request)
        # self.assertIsInstance(rval, Response)
        # self.assertEqual(rval.status, 403)

        # slf = MagicMock()
        # slf.process_file = MagicMock()
        # slf.process_file.return_value = (a,b)
