from django.test import TestCase
from file.tests import GeneralFileTest
from workspace.models import MetadataSchemas, Workspace, WorkspaceToken
import json
import datetime
from django.urls import reverse, path, include
from rest_framework.test import APITestCase, URLPatternsTestCase, APIClient
import jsonschema
from unittest.mock import MagicMock, patch, mock_open, PropertyMock
from jsonschema import Draft202012Validator

# Create your tests here.

class SchemaSetup(GeneralFileTest):
    def setUp(self):
        GeneralFileTest.setUp(self)
        self.workspace_obj2 = Workspace.objects.create()
        self.workspace_obj2.save()
        workspace_token2 = "aaf19f87-f741-4f53-ae50-a9272dc87ea7"
        self.wrksp_token2 = WorkspaceToken.objects.create(
            workspace=self.workspace_obj2,
            name="token 2",
            token=workspace_token2,
        )
        self.invalid_schema = {
            "type" : "object",
            "properties" : {
                "price" : {"type" : "number"},
                "name" : {"type" : "ggggggggggggggggg"},
            },
        }

        self.invalid_schema_str = json.dumps(self.invalid_schema)

class SchemaModelTestCase(TestCase, SchemaSetup):
    def __init__(self, *args, **kwargs):
        TestCase.__init__(self, *args, **kwargs)
        SchemaSetup.__init__(self)

    def setUp(self):
        SchemaSetup.setUp(self)


    def test_attributes(self):
        f = MetadataSchemas.objects.first()
        self.assertTrue(hasattr(f, "id"))
        self.assertTrue(hasattr(f, "workspace"))
        self.assertTrue(hasattr(f, "schema"))
        self.assertTrue(hasattr(f, "user"))
        self.assertTrue(hasattr(f, "filetype"))
        self.assertTrue(hasattr(f, "date"))
        self.assertTrue(hasattr(f, "history"))

    def test_workspace(self):
        self.assertEqual(self.schema_obj1.workspace, self.workspace_obj)

    def test_user(self):
        self.assertEqual(self.schema_obj1.user, self.user_obj)

    def test_schema(self):
        self.assertEqual(self.schema_obj1.schema, self.schema)

    def test_filetype(self):
        self.assertEqual(self.schema_obj1.filetype, self.filetype)

    def test_date(self):
        diff = self.schema_obj1.date - datetime.datetime.now(datetime.timezone.utc)
        self.assertGreater(60, diff.total_seconds())

    def test_history(self):
        h1 = self.schema_obj1.history.all()
        self.assertEqual(len(h1),1)
        MetadataSchemas.objects.filter(id=self.schema_obj1.id).update(schema=self.invalid_schema)
        self.schema_obj1.save()
        h2 = self.schema_obj1.history.all()
        self.assertEqual(len(h2),2)


class SchemaViewTests(APITestCase, URLPatternsTestCase, SchemaSetup):
    urlpatterns = [
        path("api/", include("workspace.urls")),
    ]

    def __init__(self, *args, **kwargs):
        APITestCase.__init__(self, *args, **kwargs)
        URLPatternsTestCase.__init__(self, *args, **kwargs)
        SchemaSetup.__init__(self)

    def setUp(self):
        SchemaSetup.setUp(self)

    def test_fail_workspace(self):
        self.client.credentials(HTTP_AUTHORIZATION="Token " + self.token.key)
        response = self.client.post(
            reverse('update_schema') + "?token={}".format(self.wrksp_token2.token),

            {
                "filetype": "atype",
                "metadata_schema": self.schema_str,
            }
        )
        self.assertEqual(response.status_code, 403)
        self.assertEqual(response.data, {"message": "workspace access denied"})


    def test_fail_filetype(self):
        self.client.credentials(HTTP_AUTHORIZATION="Token " + self.token.key)
        response = self.client.post(
            reverse('update_schema')+ "?token={}".format(self.wrksp_token.token),
            {
                "metadata_schema": self.schema_str,
            }
        )
        self.assertEqual(response.status_code, 400)
        self.assertEqual(response.data, {"message": "filetype field is empty"})


    def test_fail_metadata(self):
        self.client.credentials(HTTP_AUTHORIZATION="Token " + self.token.key)
        response = self.client.post(
            reverse('update_schema')+ "?token={}".format(self.wrksp_token.token),
            {
                "filetype": "atype",
            }
        )
        self.assertEqual(response.status_code, 400)
        self.assertEqual(response.data, {"message": "metadata_schema field is empty"})

    def test_create_schema(self):
        self.client.credentials(HTTP_AUTHORIZATION="Token " + self.token.key)
        response = self.client.post(
            reverse('update_schema')+ "?token={}".format(self.wrksp_token.token),

            {
                "metadata_schema": self.schema_str,
                "filetype": "btype",
            }
        )
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data["operation"], "created")
        self.assertTrue("id" in response.data)

    def test_get_schema_noauth(self):
        self.client.credentials(HTTP_AUTHORIZATION="Token " + self.token.key)
        response = self.client.get(
            reverse('update_schema')+ "?token={}".format(self.wrksp_token2.token),
        )
        self.assertEqual(response.status_code, 403)
        self.assertEqual(response.data["message"],  "workspace access denied")

    @patch('workspace.models.MetadataSchemas.objects.filter')
    @patch('workspace.models.Workspace.objects.filter')
    def test_get_schema_filetype(self, W, MS):
        with patch('workspace.views.MetadataSchemaSerializer') as MSS,\
            patch('workspace.views.WorkspaceToken.objects.get') as WT:
            wrk =  PropertyMock()
            wrk.pk = PropertyMock()
            WT.return_value.workspace = wrk
            W.return_value.exists.resturn_value = True

            MS.return_value = "DATA"
            serda = MagicMock()
            MSS.return_value.data = "serialized_data"
            self.client.credentials(HTTP_AUTHORIZATION="Token " + self.token.key)
            response = self.client.get(
                reverse('get_schema')+ "?token={}".format(self.wrksp_token.token),
            )
            self.assertEqual(response.status_code, 200)
            self.assertEqual(response.data["schemas"],  "serialized_data")
            WT.assert_called_with(token=self.wrksp_token.token)
            W.assert_called_with(pk=wrk.pk, users__pk=self.user_obj.id)
            W.return_value.exists.assert_called_once()
            MS.assert_called_with(workspace=wrk)
            MSS.assert_called_with("DATA", many=True)


            data0 = MagicMock()
            data0.filter.return_value = "DATA"
            data = MagicMock()
            data.filter.return_value = data0
            MS.return_value = data
            MS.return_value.filter.return_value = "DATA2"
            self.client.credentials(HTTP_AUTHORIZATION="Token " + self.token.key)

            response = self.client.get(
                reverse('get_schema'),
                {
                    "token":self.wrksp_token.token,
                    "filetype": "atype",
                }
            )
            self.assertEqual(response.status_code, 200)
            self.assertEqual(response.data["schemas"],  "serialized_data")

            WT.assert_called_with(token=self.wrksp_token.token)
            W.assert_called_with(pk=wrk.pk, users__pk=self.user_obj.id)
            MS.assert_called_with(workspace=wrk)
            MSS.assert_called_with("DATA2", many=True)
            data.filter.asssert_called_with(filetype="atype")

    def test_update_schema(self):
        self.client.credentials(HTTP_AUTHORIZATION="Token " + self.token.key)
        response = self.client.post(
            reverse('update_schema')+ "?token={}".format(self.wrksp_token.token),

            {
                "metadata_schema": self.schema2_str,
                "filetype": "atype",
            }
        )
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data["operation"], "updated")
        self.assertTrue("id" in response.data)

    def test_fail_json(self):
        self.client.credentials(HTTP_AUTHORIZATION="Token " + self.token.key)
        response = self.client.post(
            reverse('update_schema')+ "?token={}".format(self.wrksp_token.token),
            {
                "metadata_schema": "ddddddddddddddddddddddddddddddddd",
                "filetype": "atype",
            }
        )
        self.assertEqual(response.status_code, 400)
        self.assertEqual(response.data, {"message": "error when parsing JSON schema"})

    def test_fail_schema_format(self):
        self.client.credentials(HTTP_AUTHORIZATION="Token " + self.token.key)
        response = self.client.post(
            reverse('update_schema')+ "?token={}".format(self.wrksp_token.token),

            {
                "metadata_schema": self.invalid_schema_str,
                "filetype": "atype",
            }
        )
        try:
            schema_j = json.loads(self.invalid_schema_str)
            schema_j["$schema"] = "file://../../schema"
            Draft202012Validator.check_schema(schema_j)
        except jsonschema.exceptions.SchemaError as err:
            msg = str(err)
        self.assertEqual(response.status_code, 400)
        self.assertTrue("message" in response.data.keys())
        self.assertEqual("invalid schema format: \n {}".format(msg), response.data["message"])
