import os
import dj_database_url
dirname = os.path.dirname(os.path.abspath(__file__))

from .settings import *  # noqa: F403
# This is NOT a complete production settings file. For more, see:
# See https://docs.djangoproject.com/en/dev/howto/deployment/checklist/

DEBUG = True#@TODO switch last
ALLOWED_HOSTS = ['localhost', "storck1.cern.ch", "0.0.0.0"]
SECRET_KEY = os.environ['SECRET_KEY']
DATABASES['default'] = dj_database_url.config(conn_max_age=600)  # noqa: F405
STATIC_ROOT = os.path.join(BASE_DIR, 'static')  # noqa: F405
STATICFILES_STORAGE = 'django.contrib.staticfiles.storage.ManifestStaticFilesStorage'
# Added because of manage.py check --deploy --settings=storck.settings.deploy
#CSRF_COOKIE_SECURE=True
#X_FRAME_OPTIONS="DENY"
#SESSION_COOKIE_SECURE=True
#SECURE_SSL_REDIRECT=True
#SECURE_CONTENT_TYPE_NOSNIFF=True
#SECURE_BROWSER_XSS_FILTER=True
