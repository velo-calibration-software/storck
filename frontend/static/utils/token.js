export const removeAuthData = () => {
    localStorage.removeItem('token');
    localStorage.removeItem('workspaceToken');
};

export const setAuthData = (authData) => localStorage.setItem('token', JSON.stringify(authData));
export const setWorkspaceToken = (authData) => localStorage.setItem('workspaceToken', JSON.stringify(authData));

export const getAuthData = () => JSON.parse(localStorage.getItem('token'));
export const getWorkspaceToken = () => JSON.parse(localStorage.getItem('workspaceToken'));

window.removeAuthData = removeAuthData;
window.setAuthData = setAuthData;
window.setWorkspaceToken = setWorkspaceToken;
window.getAuthData = getAuthData;
window.getWorkspaceToken = getWorkspaceToken;
